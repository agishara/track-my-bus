package org.hms.repo;

import org.hms.domain.entity.Bus;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

@Repository
public interface BusRepo extends CrudRepository<Bus, String> {

    @Modifying(clearAutomatically = true)
    @Query("update Bus b set b.latitude = :latitude, b.longitude = :longitude where b.busNumber = :busNumber")
    void updateBusLocation(@Param("latitude") BigDecimal latitude, @Param("longitude") BigDecimal longitude, @Param("busNumber") String busNumber);

}
