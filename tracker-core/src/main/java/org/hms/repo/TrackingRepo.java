package org.hms.repo;

import org.hms.domain.BusTiming;
import org.hms.domain.BusesByStopDto;

import java.util.List;

public interface TrackingRepo {

    List<BusesByStopDto> findBusesComingToStop(Long busStopId);

}
