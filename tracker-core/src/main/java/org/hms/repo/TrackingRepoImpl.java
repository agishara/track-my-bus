package org.hms.repo;

import org.hms.domain.BusTiming;
import org.hms.domain.BusesByStopDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class TrackingRepoImpl implements TrackingRepo{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final String QUERY_STRING_TIMING = "SELECT " +
            "  bs.stop_id, " +
            "  bs.description, " +
            "  bs.latitude, " +
            "  bs.longitude, " +
            "  b.latitude  AS bus_lat, " +
            "  b.longitude AS bus_long, " +
            "  r.route_name " +
            "FROM bus_stop bs LEFT JOIN bus b ON b.route_id = bs.route_id " +
            "  LEFT JOIN route AS r ON r.route_id = bs.route_id " +
            "WHERE bs.stop_id = %s;";

    private final String QUERY_TIMING = "SELECT " +
            "  stop_id," +
            "  sub.description," +
            "  stop_lat," +
            "  stop_long," +
            "  route_name," +
            "  latitude," +
            "  longitude " +
            "FROM (SELECT " +
            "        bs.stop_id," +
            "        bs.description," +
            "        bs.latitude  AS stop_lat," +
            "        bs.longitude AS stop_long," +
            "        rbs.route_id " +
            "      FROM bus_stop bs LEFT JOIN route_bus_stop rbs ON bs.stop_id = rbs.stop_id " +
            "      WHERE bs.stop_id = %s) AS sub LEFT JOIN bus b ON b.route_id = sub.route_id " +
            "  LEFT JOIN route r ON r.route_id = sub.route_id;";

    @Override
    public List<BusesByStopDto> findBusesComingToStop(Long busStopId) {
        List<BusesByStopDto> result = jdbcTemplate.query(String.format(QUERY_TIMING, busStopId), new Object[]{}, new BusesByStopDtoRowMapper());
        return result;
    }
}

class BusesByStopDtoRowMapper implements RowMapper<BusesByStopDto>
{
    @Override
    public BusesByStopDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        BusesByStopDto result = new BusesByStopDto(
                rs.getLong("stop_id"),
                rs.getString("description"),
                rs.getBigDecimal("stop_lat"),
                rs.getBigDecimal("stop_long"),
                rs.getBigDecimal("latitude"),
                rs.getBigDecimal("longitude"),
                rs.getString("route_name")
        );
        return result;
    }
}
