package org.hms.repo;

import org.hms.domain.entity.Route;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RouteRepo extends CrudRepository<Route, Long> {

    @Query("select r from Route r left join fetch r.busStops bs where r.routeId = ?1")
    Route findOneWithBusStops(int routeId);
}
