package org.hms.repo;

import org.hms.domain.BusesByStopDto;
import org.hms.domain.entity.BusStop;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface BusStopRepo extends CrudRepository<BusStop, Integer> {

    @Query("select bs from BusStop bs where bs.latitude >= ?1 and bs.latitude <= ?2 and bs.longitude >= ?3 and bs.longitude <= ?4")
    List<BusStop> findNearestBusStops(BigDecimal latitudeMin, BigDecimal latitudeMax, BigDecimal longitudeMin, BigDecimal longitudeMax);

    /*@Query(name = "SELECT " +
            "  bs.stop_id, " +
            "  bs.description, " +
            "  bs.latitude, " +
            "  bs.longitude, " +
            "  b.latitude  AS bus_lat, " +
            "  b.longitude AS bus_long, " +
            "  r.route_name " +
            "FROM bus_stop bs LEFT JOIN bus b ON b.route_id = bs.route_id " +
            "  LEFT JOIN route AS r ON r.route_id = bs.route_id " +
            "WHERE bs.stop_id = 2;", nativeQuery = true)
    List<Object[]> findBusesComingToStop(Long busStopId);*/
}
