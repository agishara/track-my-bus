package org.hms.domain;

import org.hms.domain.entity.BusStop;

import java.util.List;

public class BusStopListRes {
    private List<BusStop> busStops;

    public BusStopListRes(List<BusStop> busStops) {
        this.busStops = busStops;
    }

    public List<BusStop> getBusStops() {
        return busStops;
    }

    public void setBusStops(List<BusStop> busStops) {
        this.busStops = busStops;
    }
}
