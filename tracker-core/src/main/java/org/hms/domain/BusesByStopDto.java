package org.hms.domain;

import java.math.BigDecimal;

public class BusesByStopDto implements Comparable{
    private Long busStopId;
    private String busStopDescription;
    private BigDecimal busStopLatitude;
    private BigDecimal busStopLongitude;
    private BigDecimal busLatitude;
    private BigDecimal busLongitude;
    private BigDecimal timeToReachInMinute;
    private String routeName;

    public BusesByStopDto(Long busStopId, String busStopDescription, BigDecimal busStopLatitude,
                          BigDecimal busStopLongitude, BigDecimal busLatitude, BigDecimal busLongitude,
                          String routeName) {
        this.busStopId = busStopId;
        this.busStopDescription = busStopDescription;
        this.busStopLatitude = busStopLatitude;
        this.busStopLongitude = busStopLongitude;
        this.busLatitude = busLatitude;
        this.busLongitude = busLongitude;
        this.routeName = routeName;
    }

    public Long getBusStopId() {
        return busStopId;
    }

    public void setBusStopId(Long busStopId) {
        this.busStopId = busStopId;
    }

    public String getBusStopDescription() {
        return busStopDescription;
    }

    public void setBusStopDescription(String busStopDescription) {
        this.busStopDescription = busStopDescription;
    }

    public BigDecimal getBusStopLatitude() {
        return busStopLatitude;
    }

    public void setBusStopLatitude(BigDecimal busStopLatitude) {
        this.busStopLatitude = busStopLatitude;
    }

    public BigDecimal getBusStopLongitude() {
        return busStopLongitude;
    }

    public void setBusStopLongitude(BigDecimal busStopLongitude) {
        this.busStopLongitude = busStopLongitude;
    }

    public BigDecimal getBusLatitude() {
        return busLatitude;
    }

    public void setBusLatitude(BigDecimal busLatitude) {
        this.busLatitude = busLatitude;
    }

    public BigDecimal getBusLongitude() {
        return busLongitude;
    }

    public void setBusLongitude(BigDecimal busLongitude) {
        this.busLongitude = busLongitude;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public BigDecimal getTimeToReachInMinute() {
        return timeToReachInMinute;
    }

    public void setTimeToReachInMinute(BigDecimal timeToReachInMinute) {
        this.timeToReachInMinute = timeToReachInMinute;
    }

    @Override
    public int compareTo(Object o) {
        BusesByStopDto dto = (BusesByStopDto) o;
//        return this.timeToReachInMinute.compareTo( dto.timeToReachInMinute);
        return dto.timeToReachInMinute.compareTo( this.timeToReachInMinute);
    }
}
