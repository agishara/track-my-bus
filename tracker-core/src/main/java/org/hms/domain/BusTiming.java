package org.hms.domain;

import java.math.BigDecimal;

public class BusTiming {
    private String busRoute;
    private BigDecimal timeInMinToFirst;
    private BigDecimal timeInMinToSecond;
    private BigDecimal timeInMinToThird;

    public BusTiming(String busRoute, BigDecimal timeInMinToFirst, BigDecimal timeInMinToSecond, BigDecimal timeInMinToThird) {
        this.busRoute = busRoute;
        this.timeInMinToFirst = timeInMinToFirst;
        this.timeInMinToSecond = timeInMinToSecond;
        this.timeInMinToThird = timeInMinToThird;
    }

    public String getBusRoute() {
        return busRoute;
    }

    public void setBusRoute(String busRoute) {
        this.busRoute = busRoute;
    }

    public BigDecimal getTimeInMinToFirst() {
        return timeInMinToFirst;
    }

    public void setTimeInMinToFirst(BigDecimal timeInMinToFirst) {
        this.timeInMinToFirst = timeInMinToFirst;
    }

    public BigDecimal getTimeInMinToSecond() {
        return timeInMinToSecond;
    }

    public void setTimeInMinToSecond(BigDecimal timeInMinToSecond) {
        this.timeInMinToSecond = timeInMinToSecond;
    }

    public BigDecimal getTimeInMinToThird() {
        return timeInMinToThird;
    }

    public void setTimeInMinToThird(BigDecimal timeInMinToThird) {
        this.timeInMinToThird = timeInMinToThird;
    }
}
