package org.hms.domain.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "bus_stop")
public class BusStop implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer stopId;

    @Column(name = "description")
    private String description;

    @Column(name = "latitude",precision = 7, scale = 4)
    private BigDecimal latitude;

    @Column(name = "longitude",precision = 7, scale = 4)
    private BigDecimal longitude;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "busStops")
    private List<Route> routes;

    @Transient
    private BigDecimal distance;

    public BusStop() {
    }

    public BusStop(Integer stopId, String description, BigDecimal latitude, BigDecimal longitude) {
        this.stopId = stopId;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    /*public BusStop(Integer stopId, String description, BigDecimal latitude, BigDecimal longitude, BigDecimal distance) {
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
        this.route = route;
        this.distance = distance;
    }*/

    public Integer getStopId() {
        return stopId;
    }

    public void setStopId(Integer stopId) {
        this.stopId = stopId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }
}
