package org.hms.domain.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "route")
public class Route implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer routeId;

    @Column(name = "route_name")
    private String routeName;

    @Column(name = "description")
    private String description;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "route_bus_stop", joinColumns = {
            @JoinColumn(name = "route_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "stop_id",
                    nullable = false, updatable = false) })
    private List<BusStop> busStops;

    public Route() {
    }

    public Route(String routeName, String description, List<BusStop> busStops) {
        this.routeName = routeName;
        this.description = description;
        this.busStops = busStops;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<BusStop> getBusStops() {
        return busStops;
    }

    public void setBusStops(List<BusStop> busStops) {
        this.busStops = busStops;
    }
}
