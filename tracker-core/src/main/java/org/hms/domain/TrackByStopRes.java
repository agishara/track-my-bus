package org.hms.domain;

import java.time.LocalDateTime;
import java.util.List;

public class TrackByStopRes {
    private List<BusTiming> timings;
    private LocalDateTime time;

    public TrackByStopRes(List<BusTiming> timings, LocalDateTime time) {
        this.timings = timings;
        this.time = time;
    }

    public List<BusTiming> getTimings() {
        return timings;
    }

    public void setTimings(List<BusTiming> timings) {
        this.timings = timings;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
