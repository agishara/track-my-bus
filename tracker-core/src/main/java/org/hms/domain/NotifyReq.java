package org.hms.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by ishara on 2/10/18.
 */
public class NotifyReq {
    private String busNumber;
    private BigDecimal latitude;
    private BigDecimal longitude;
    private LocalDateTime updatedTime;

    public NotifyReq() {
    }

    public NotifyReq(String busNumber, BigDecimal latitude, BigDecimal longitude, LocalDateTime updatedTime) {
        this.busNumber = busNumber;
        this.latitude = latitude;
        this.longitude = longitude;
        this.updatedTime = updatedTime;
    }

    public String getBusNumber() {
        return busNumber;
    }

    public void setBusNumber(String busNumber) {
        this.busNumber = busNumber;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    public LocalDateTime getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(LocalDateTime updatedTime) {
        this.updatedTime = updatedTime;
    }
}
