package org.hms.domain;

import java.time.LocalDateTime;

/**
 * Created by ishara on 2/10/18.
 */
public class NotifyRes {
    private String statusCode;
    private String description;
    private LocalDateTime time;

    public NotifyRes(String statusCode, String description, LocalDateTime time) {
        this.statusCode = statusCode;
        this.description = description;
        this.time = time;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
