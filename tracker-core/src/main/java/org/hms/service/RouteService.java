package org.hms.service;

import org.hms.domain.entity.BusStop;
import org.hms.domain.entity.Route;

import java.util.List;

public interface RouteService {

    List<BusStop> findBusStopsByRoute(int routeId);

}
