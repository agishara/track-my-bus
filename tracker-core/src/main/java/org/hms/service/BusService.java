package org.hms.service;

import java.math.BigDecimal;

public interface BusService {

    void updateBusLocation(BigDecimal latitude, BigDecimal longitude, String busNumber);

}
