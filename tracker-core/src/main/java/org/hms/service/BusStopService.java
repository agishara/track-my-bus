package org.hms.service;

import org.hms.domain.BusTiming;
import org.hms.domain.entity.BusStop;

import java.math.BigDecimal;
import java.util.List;

public interface BusStopService {

    List<BusStop> retrieveNearestBusStops(BigDecimal latitude, BigDecimal longitude);

    List<BusTiming> retrieveBusesByStop(Long busStopId);
}
