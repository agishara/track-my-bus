package org.hms.service.impl;

import org.hms.domain.BusTiming;
import org.hms.domain.BusesByStopDto;
import org.hms.domain.entity.BusStop;
import org.hms.repo.BusStopRepo;
import org.hms.repo.TrackingRepo;
import org.hms.service.BusStopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class BusStopServiceImpl implements BusStopService {

    @Autowired
    private BusStopRepo busStopRepo;

    @Autowired
    private TrackingRepo trackingRepo;

    /**
     * Find nearest bus stops in 0.01 square degree
     * @param latitude
     * @param longitude
     * @return
     */
    @Override
    public List<BusStop> retrieveNearestBusStops(BigDecimal latitude, BigDecimal longitude) {
        BigDecimal degree = new BigDecimal(0.1);
        List<BusStop> busStops = busStopRepo.findNearestBusStops(
                latitude.subtract(degree),
                latitude.add(degree),
                longitude.subtract(degree),
                longitude.add(degree));

        return busStops.stream()
                .map(
                        n -> {
                            BusStop stop = new BusStop(n.getStopId(), n.getDescription(), n.getLatitude(), n.getLongitude());
                            stop.setDistance(getDistance(latitude, longitude, n.getLatitude(), n.getLongitude()));
                            return stop;
                        }
                ).collect(Collectors.toList());
    }

    private BigDecimal getDistance(BigDecimal latitude, BigDecimal longitude, BigDecimal stopLatitude, BigDecimal stopLongitude) {
        BigDecimal distance = new BigDecimal(
                distance(latitude.doubleValue(), longitude.doubleValue(), stopLatitude.doubleValue(), stopLongitude.doubleValue(), "K")*1000
        );
        return new BigDecimal(Math.round(distance.doubleValue()));
    }

    @Override
    public List<BusTiming> retrieveBusesByStop(Long busStopId) {

        final double busSpeedInMM = 40.0;//speed meters per minute
        List<BusesByStopDto> busesComingToStop = trackingRepo.findBusesComingToStop(busStopId);

        //TODO change the logic to perform better
        for (BusesByStopDto bus : busesComingToStop) {
            double distanceImMeters = distance(
                    bus.getBusLatitude().doubleValue(),
                    bus.getBusLongitude().doubleValue(),
                    bus.getBusStopLatitude().doubleValue(),
                    bus.getBusStopLongitude().doubleValue(), "K")*1000;
            double timeInMinute = distanceImMeters/busSpeedInMM;
            timeInMinute = Math.round(timeInMinute);
            bus.setTimeToReachInMinute(new BigDecimal(timeInMinute));
        }

        Collections.sort(busesComingToStop);

        Map<String, List<BigDecimal>> busesByRoute = new HashMap<>();
        for (BusesByStopDto bus : busesComingToStop) {
            if(busesByRoute.containsKey(bus.getRouteName())) {
                List<BigDecimal> busTimings = busesByRoute.get(bus.getRouteName());
                if(busTimings==null) {
                    busTimings = new ArrayList<BigDecimal>();
                }
                List<BigDecimal> newTimings = new ArrayList<>();
                newTimings.add(bus.getTimeToReachInMinute());
                newTimings.addAll(busTimings);
                busesByRoute.put(bus.getRouteName(), newTimings);
            } else {
                busesByRoute.put(bus.getRouteName(), Arrays.asList(bus.getTimeToReachInMinute()));
            }
        }

        List<BusTiming> timings = new ArrayList<>();
        for (Map.Entry<String, List<BigDecimal>> listEntry : busesByRoute.entrySet()) {
            if(listEntry.getValue().size()>3) {
                listEntry.setValue(listEntry.getValue().subList(0,4));
            }

            if(listEntry.getValue().size() >= 3) {
                timings.add(
                        new BusTiming(listEntry.getKey(), listEntry.getValue().get(0), listEntry.getValue().get(1), listEntry.getValue().get(2))
                );
            } else if(listEntry.getValue().size() == 2) {
                timings.add(
                        new BusTiming(listEntry.getKey(), listEntry.getValue().get(0), listEntry.getValue().get(1), null)
                );
            } else if(listEntry.getValue().size() == 1) {
                timings.add(
                        new BusTiming(listEntry.getKey(), listEntry.getValue().get(0), null, null)
                );
            }
        }

        return timings;
    }

    private static double distance(double lat1, double lon1, double lat2, double lon2, String unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "N") {
            dist = dist * 0.8684;
        }
        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

}
