package org.hms.service.impl;

import org.hms.domain.entity.BusStop;
import org.hms.domain.entity.Route;
import org.hms.repo.RouteRepo;
import org.hms.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RouteServiceImpl implements RouteService {

    @Autowired
    private RouteRepo routeRepo;

    @Override
    public List<BusStop> findBusStopsByRoute(int routeId) {
        Route route = routeRepo.findOneWithBusStops(routeId);
        return route.getBusStops().stream()
                .map(
                        n -> new BusStop(n.getStopId(), n.getDescription(), n.getLatitude(), n.getLongitude())
                ).collect(Collectors.toList());
    }

}
