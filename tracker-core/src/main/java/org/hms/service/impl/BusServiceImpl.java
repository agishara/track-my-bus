package org.hms.service.impl;

import org.hms.domain.entity.Bus;
import org.hms.repo.BusRepo;
import org.hms.service.BusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Service
@Transactional
public class BusServiceImpl implements BusService {

    @Autowired
    private BusRepo busRepo;

    @Override
    public void updateBusLocation(BigDecimal latitude, BigDecimal longitude, String busNumber) {
        busRepo.updateBusLocation(latitude, longitude, busNumber);
    }
}
