package org.hms.tracker;

import org.hms.domain.*;
import org.hms.domain.entity.BusStop;
import org.hms.domain.entity.Route;
import org.hms.service.BusService;
import org.hms.service.BusStopService;
import org.hms.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ishara on 2/10/18.
 */

@Controller
public class NotificationController {

    @Autowired
    private RouteService routeService;

    @Autowired
    private BusService busService;

    @Autowired
    private BusStopService busStopService;

    @PostMapping(path = "/receive/notification")
    public @ResponseBody NotifyRes receiveNotifications(@RequestBody NotifyReq request) {
        busService.updateBusLocation(
                request.getLatitude(),
                request.getLongitude(),
                request.getBusNumber()
                );
        return new NotifyRes("1000", "Success",LocalDateTime.now());
    }

    @GetMapping(path = "/retrieve/buses/{busStopId}")
    public @ResponseBody
    TrackByStopRes retrieveBusesByStop(@PathVariable("busStopId") Long busStopId) {

        return new TrackByStopRes(busStopService.retrieveBusesByStop(busStopId), LocalDateTime.now());

    }

    @GetMapping(path = "/retrieve/bus/stop/list/{routeId}")
    public @ResponseBody BusStopListRes retrieveBusStopListByRoute(@PathVariable("routeId") Integer routeId) {

        return new BusStopListRes(routeService.findBusStopsByRoute(routeId));

    }

    @GetMapping(path = "/retrieve/nearest/bus/stop/list/{latitude}/{longitude}")
    public @ResponseBody BusStopListRes retrieveNearestBusStops(
            @PathVariable("latitude") BigDecimal latitude,
            @PathVariable("longitude") BigDecimal longitude) {

        return new BusStopListRes(busStopService.retrieveNearestBusStops(latitude, longitude));

    }

}
