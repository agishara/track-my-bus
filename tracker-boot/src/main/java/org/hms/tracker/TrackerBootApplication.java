package org.hms.tracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("org.hms.*")
@ComponentScan(basePackages = { "org.hms.*" })
@EntityScan("org.hms.*")
public class TrackerBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrackerBootApplication.class, args);
	}

}
