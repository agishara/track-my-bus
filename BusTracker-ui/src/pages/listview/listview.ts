import { Component, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Busstop } from './../../models/busstop';
import { Bus} from './../../models/bus';
import {Buses} from '../../providers/providers';
import { Posts } from '../../providers/providers';
import { Busstops } from '../../providers/providers';
import { AnimationService ,AnimationBuilder} from 'css-animator';

/**
 * Generated class for the ListviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listview',
  templateUrl: 'listview.html',
   animations: [
    trigger('myvisibility', [
      state('void', style({ opacity: '0' })),
      state('*', style({ opacity: '1' })),
      transition('void <=> *', animate('1050ms ease-in'))
    ])
  ]
})
export class ListviewPage {
  currentBuses: any = [];
  currentBusstops: any = [];
  postList :any = [];
  private animator : AnimationBuilder;
  visibility = 'void';
  @ViewChild('myElement') myElement;
  constructor(public navCtrl: NavController, public navParams: NavParams,public buses: Buses,public busstops: Busstops,public posts : Posts,animationservice :AnimationService) {
    this.animator = animationservice.builder();
    this.currentBuses = this.buses.query();
     this.busstops.nenearestBustops("/7.2896/80.6308/").then((response:any)=>{
     this.currentBusstops = response;
    // this.visibility = (this.visibility == 'void')?'*' : 'void';
 
   
    });

   
  }

animateList(){
  
  this.animator.setType('wobble').show(this.myElement.nativeElement)
  console.log("animateions")
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ListviewPage');
   
  }

  getBusstops(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentBusstops = [];
      return;
    }
    this.currentBusstops = this.buses.query({
      busRoute: val
    });
  }

 
  getBuses(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentBuses = [];
      return;
    }
    this.currentBuses = this.buses.query({
      busRoute: val
    });
  }



  /**
   * Navigate to the detail page for this item.
   */
  openBuses(bus: Bus) {
    this.navCtrl.push('BusDetailPage', {
      bus: bus
    });
  }

   openBusstops(busstop: Busstop) {
    this.navCtrl.push('BusHaltsDetailPage', {
      busstop: busstop
    });
  }

}
