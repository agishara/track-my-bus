import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusHaltsDetailPage } from './bus-halts-detail';
import { MapPage } from "../map/map";

@NgModule({
  declarations: [
    BusHaltsDetailPage,
    MapPage
  ],
  imports: [
    IonicPageModule.forChild(BusHaltsDetailPage),
  ],
})
export class BusHaltsDetailPageModule {}
