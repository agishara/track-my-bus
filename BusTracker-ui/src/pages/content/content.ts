import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {  AnimationService } from "css-animator";
import { AnimationBuilder } from 'css-animator/builder';
@IonicPage()
@Component({
  selector: 'page-content',
  templateUrl: 'content.html'
})
export class ContentPage { @ViewChild('myElement') myElem;
  private animator: AnimationBuilder;
 
  constructor(public navCtrl: NavController, animationService: AnimationService) {
    this.animator = animationService.builder();
  }

   animateElem() {
    this.animator.setType('flipInX').show(this.myElem.nativeElement);
  }


}
