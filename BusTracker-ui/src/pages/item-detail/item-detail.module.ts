import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import {MatExpansionModule} from '@angular/material/expansion';
import { ItemDetailPage } from './item-detail';
import { CountdownPage } from "../countdown/countdown";

import {MatButtonToggleModule} from '@angular/material/button-toggle';
@NgModule({
  declarations: [
    ItemDetailPage,
    CountdownPage,

  ],
  imports: [
    IonicPageModule.forChild(ItemDetailPage),
    TranslateModule.forChild()
  ],
  exports: [
    ItemDetailPage,
    MatButtonToggleModule
  ]
})
export class ItemDetailPageModule { }
