import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Routes } from '../../providers/providers';
import { Busstops } from '../../providers/providers';
import { Buses } from '../../providers/providers';
import { query } from '\@angular/animations';
import { Bus } from "../../models/bus";
import { Busstop } from './../../models/busstop';

@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage {
  route: any;
  busStops : any = [];
  buses : any = [];
  showLevel1 = null;
  showLevel2 = null;
  constructor(public navCtrl: NavController, navParams: NavParams, routes: Routes,public busstop : Busstops,public bus : Buses) {
    this.route = navParams.get('route') || routes.defaultItem;
    
  }

 
  ionViewDidLoad() {
    console.log('ionViewDidLoad CountdownPage');
   this.getBuses();
    this.getBusstops(this.route.busRoute)
    
  }

  getBusstops(busRoute){
     this.busstop.query(busRoute).then((response:any)=>{
      this.busStops = response;
        console.log(this.busStops);
    });
    
    
  }

     openBusstops(busstop: Busstop) {
    this.navCtrl.push('BusHaltsDetailPage', {
      busstop: busstop
    });
  }
  getBuses(){
    this.buses = this.bus.query();
  }
  openBuses(bus: Bus) {
    this.navCtrl.push('BusDetailPage', {
      bus: bus
    });
  }

  toggleLevel1(idx) {
  if (this.isLevel1Shown(idx)) {
    this.showLevel1 = null;
  } else {
    this.showLevel1 = idx;
  }
};

toggleLevel2(idx) {
  if (this.isLevel2Shown(idx)) {
    this.showLevel1 = null;
    this.showLevel2 = null;
  } else {
    this.showLevel1 = idx;
    this.showLevel2 = idx;
  }
};

isLevel1Shown(idx) {
  return this.showLevel1 === idx;
};

isLevel2Shown(idx) {
  return this.showLevel2 === idx;
};
}

