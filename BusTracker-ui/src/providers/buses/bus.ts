import { Injectable } from '@angular/core';

import { Api } from '../api/api';
import { Bus } from './../../models/bus';

@Injectable()
export class Buses {

  constructor(public api: Api) { }

  query(params?: any) {
    return this.api.get('/retrieve/nearest/bus/stop/list/{latitude}/{longitude}', params);
  }

  add(busstop: Bus) {
  }

  delete(busstop: Bus) {
  }

}
