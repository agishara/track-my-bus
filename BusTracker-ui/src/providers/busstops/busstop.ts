import { Injectable } from '@angular/core';

import { Api } from '../api/api';
import { Busstop } from './../../models/busstop';

@Injectable()
export class Busstops {

  constructor(public api: Api) { }

query(params?: string) {
       return new Promise((resolve: any, _reject: any)=>{
      setTimeout(()=>{
      this.api.get('/bus/stop/list/',params).subscribe((data:any)=>{
        resolve(data.busStops);
      });},1000);
    });
  }

  nenearestBustops(params?: string) {
       return new Promise((resolve: any, _reject: any)=>{
      setTimeout(()=>{
      this.api.get('nearest/bus/stop/list/',params).subscribe((data:any)=>{
        resolve(data.busStops);
      });},1000);
    });
  }

getTimeFromBusStop(params?: any){
 return new Promise((resolve: any, _reject: any)=>{
      setTimeout(()=>{
      this.api.get('buses/',params).subscribe((data:any)=>{
        resolve(data.timings);
      });},1000);
    });
}

}
