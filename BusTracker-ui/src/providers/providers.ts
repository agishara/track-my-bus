import { Api } from './api/api';
import { Items } from '../mocks/providers/items';
import { Settings } from './settings/settings';
import { User } from './user/user';
import { Routes } from "../mocks/providers/routes";

import { GoogleMaps } from "@ionic-native/google-maps";
import { MapPage } from './../pages/map/map';
import { Busstops } from './busstops/busstop';
import { Buses } from "../mocks/providers/buses";
import { Posts} from "./posts/post";

export {
    Api,
    Items,
    Settings,
    User,
    Routes,
    GoogleMaps,
    Busstops,
    Buses,
    Posts,
    MapPage
    
};
