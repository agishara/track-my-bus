import { Injectable } from '@angular/core';

import { Post } from '../../models/post';
import { Api } from '../api/api';

@Injectable()
export class Posts {
  posts :any = [];
  constructor(public api: Api) { }

  getPosts(params?: any) {
    
    return new Promise((resolve: any, _reject: any)=>{
      setTimeout(()=>{
      this.api.get('posts', params).subscribe((data:any)=>{
        resolve(data);
      });},1000);
    });
    
  }
}
