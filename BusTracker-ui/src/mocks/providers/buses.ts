import { Injectable } from '@angular/core';
import { Bus} from './../../models/bus';
@Injectable()
export class Buses {
  buses: Bus[] = [];

  defaultItem: any = {
    "busRoute":"120",
    "numberplate": "WP 12564",
    "eta": "12 minutes",

  };


  constructor() {
    let buses  = [
      {
      "busRoute":"120",
      "numberplate": "WP 12564",
      "eta": "12 min"

    },
     {
      "busRoute":"138",
      "numberplate": "QL 9575",
      "eta": "1 hour"

    },
     {
      "busRoute":"154",
      "numberplate": "ND 2484",
      "eta": " 1h 12m"
    },
    {
      "busRoute":"154",
      "numberplate": "ND 2484",
      "eta": " 1h 12m"
    },
    {
      "busRoute":"154",
      "numberplate": "ND 2484",
      "eta": " 1h 12m"
    }
    ];

    for (let bus of buses) {
      this.buses.push(new Bus(bus));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.buses;
    }

    return this.buses.filter((bus) => {
      for (let key in params) {
        let field = bus[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return bus;
        } else if (field == params[key]) {
          return bus;
        }
      }
      return null;
    });
  }

  add(bus: Bus) {
    this.buses.push(bus);
  }

  delete(bus: Bus) {
    this.buses.splice(this.buses.indexOf(bus), 1);
  }
}
