import { Injectable } from '@angular/core';
import { Route } from './../../models/route';

@Injectable()
export class Routes {
  routes: Route[] = [];

  defaultItem: any = {
    "busRoute": "120",
    "startLocation": { "lat": "", "lang": "" },
    "endLocation": { "lat": "", "lang": "" },

  };


  constructor() {
    let routes = [
      {
        "busRoute": "120",
        "startLocation": { "name":"කැස්බෑව", "lat": "", "lang": "" },
        "endLocation": { "name":"පිටකොටුව","lat": "", "lang": "" },
      },
      {
        "busRoute": "154",
         "startLocation": { "name":"අඟුලාන", "lat": "", "lang": "" },
        "endLocation": { "name":"කිරිබත්ගොඩ","lat": "", "lang": "" },
      },
      {
        "busRoute": "17",
        "startLocation": { "name":"පානදුර", "lat": "", "lang": "" },
        "endLocation": { "name":"මහනුවර","lat": "", "lang": "" },
      },
      {
        "busRoute": "1",
        "startLocation": { "name":"Colombo", "lat": "", "lang": "" },
        "endLocation": { "name":"Kandy","lat": "", "lang": "" },
      }




    ];

    for (let route of routes) {
      this.routes.push(new Route(route));
    }
  }

  query(params?: any) {
    if (!params) {
      return this.routes;
    }

    return this.routes.filter((route) => {
      for (let key in params) {
        let field = route[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return route;
        } else if (field == params[key]) {
          return route;
        }
      }
      return null;
    });
  }

  add(route: Route) {
    this.routes.push(route);
  }

  delete(route: Route) {
    this.routes.splice(this.routes.indexOf(route), 1);
  }
}
