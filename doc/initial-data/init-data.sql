INSERT INTO route (route_id, description, route_name) VALUE (1, "Fort - Kandy", "Route-01");
INSERT INTO route (route_id, description, route_name) VALUE (2, "Fort - Kottawa", "Route-138");
INSERT INTO route (route_id, description, route_name) VALUE (3, "Colombo - Meegoda", "Route-190");
INSERT INTO route (route_id, description, route_name) VALUE (4, "Kandy - Mawanalla", "Route-777");

INSERT INTO bus_stop (stop_id, description, latitude, longitude) VALUES
(1, "Good Shed", 7.2896, 80.6308),
(2, "Kandy Hospital Bus Stop", 7.2881, 80.6315),
(3, "Deiyannewela Bus Stop", 7.2868, 80.6292),
(4, "Randles Hill Railway Station", 7.2790, 80.6180),
(5,"vision bus stop",7.2752, 80.6123),
(6,"gatambe bridge", 7.2729, 80.6041),
(7,"peradeniya",7.2656, 80.5927),
(8,"polgahamulla",7.2654, 80.5842),
(9,"kiribathkumbura",7.2675, 80.5758),
(10,"nanuoya", 7.2671, 80.5711),
(11,"embillegamee",7.2652, 80.5630),
(12,"pilimathalawa",7.2656, 80.5547),
(13,"illukwatttha",7.2611, 80.5403),
(14,"henawala",7.2620, 80.5311),
(15,"kadugannawa",7.2572, 80.5198),
(16,"hingula",7.2475, 80.4680),
(17,"mawanalla depot",7.2507, 80.4514),
(18,"mawanala CMB stop",7.2531, 80.4483),
(19,"uthuwankanda",7.2514, 80.4236),
(20,"karandupana",7.2526, 80.3769),
(21,"prison temple",7.2549, 80.3531),
(22,"kegalla central",7.2531, 80.3479),
(23,"kegalle town",7.2528, 80.3419),
(24,"ranwala",7.2507, 80.3322),
(25,"udukumbura",7.2314, 80.2438),
(26,"warakapola",7.2240, 80.1960),
(27,"pasyala",7.1678, 80.1245),
(28,"nittambuwa",7.1452, 80.0959),
(29,"malwatta", 7.1301, 80.0752),
(30,"tihariya", 7.1272, 80.0718),
(31,"kalagedihena",7.1186, 80.0598),
(32,"sapugastenna", 7.1164, 80.0566),
(33,"kalagedihena temple",7.1133, 80.0533),
(34,"bogamuwa",7.1106, 80.0511),
(35,"wisse kanuwa",7.1077, 80.0451)
;

INSERT INTO bus (bus_number, description, latitude, longitude, status, type, last_updated_time, route_id) VALUES
  ("1212", "CTB", 7.2896, 80.6308, "ACTIVE", "NORMAL", "2018-02-17T23:43", 1),
  ("1213", "PRIVATE", 7.2881, 80.6315, "ACTIVE", "NORMAL", "2018-02-17T23:43", 4),
  ("222", "PRIVATE", 7.1077, 80.0451, "ACTIVE", "NORMAL", "2018-02-17T23:43", 1),
  ("333", "PRIVATE", 7.2868, 80.6292, "ACTIVE", "A/C", "2018-02-17T23:43", 1),
  ("444", "PRIVATE", 7.2868, 80.6292, "ACTIVE", "A/C", "2018-02-17T23:43", 1),
  ("555", "CTB", 7.2896, 80.6308, "ACTIVE", "NORMAL", "2018-02-17T23:43", 1),
  ("666", "PRIVATE", 7.2881, 80.6315, "ACTIVE", "NORMAL", "2018-02-17T23:43", 1),
  ("777", "PRIVATE", 7.2868, 80.6292, "ACTIVE", "A/C", "2018-02-17T23:43", 2),
  ("888", "PRIVATE", 7.2868, 80.6292, "ACTIVE", "A/C", "2018-02-17T23:43", 3)
;


insert into route_bus_stop (route_id,stop_id) values (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23),(1,24),(1,25),(1,26),(1,27),(1,28),(1,29),(1,30),(1,31),(1,32),(1,33),(1,34),(1,35);
insert into route_bus_stop (route_id,stop_id) values(2,1);
insert into route_bus_stop (route_id,stop_id) values(2,2);
insert into route_bus_stop (route_id,stop_id) values(2,6);
insert into route_bus_stop (route_id,stop_id) values(3,6);
insert into route_bus_stop (route_id,stop_id) values (4,2),(4,3),(4,4),(4,5),(4,6),(4,7),(4,8),(4,9),(4,10),(4,11),(4,12),(4,13),(4,14),(4,15),(4,16),(4,17);
