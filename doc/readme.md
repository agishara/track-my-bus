    # Git location
git@gitlab.com:agishara/track-my-bus.git

# Notification message request
curl -d '{"busNumber":"1212", "longitude":"10.1210","latitude":"79.2323", "updatedTime":"2018-02-13 00:00:00"}' -H "Content-Type: application/json" -X POST http://localhost:8080/receive/notification

# Retrieve buses by stop
curl -H "Content-Type: application/json" -X GET http://localhost:8080/retrieve/buses/{busStopId}

# Retrieve bus stops by route
curl -H "Content-Type: application/json" -X GET http://localhost:8080/retrieve/bus/stop/list/{routeId}

# Retrieve nearest bus stops
curl -H "Content-Type: application/json" -X GET http://localhost:8080/retrieve/nearest/bus/stop/list/{latitude}/{longitude}/

# Please refer the project code for request/response formats