input_data_file=bus1212_kandy_cmb.csv
service_url='http://127.0.0.1:8080/receive/notification'
time_factor=2

cat $input_data_file | while read lines
do
time_pause=`echo $lines | cut -d',' -f1`
bus_num=`echo $lines | cut -d',' -f2`
location_name=`echo $lines | cut -d',' -f3`
location_name=`echo $lines | cut -d',' -f3`
lat_num=`echo $lines | cut -d',' -f4`
long_num=`echo $lines | cut -d',' -f5`
update_time=`date +"%Y-%m-%d %H:%M:%S"`
((time_pause=time_pause*time_factor))

echo "[$update_time] bus $bus_num is now at $location_name @ $lat_num/$long_num, next update in $time_pause seconds"
curl -d "{\"busNumber\":\"$bus_num\", \"longitude\":\"$long_num\",\"latitude\":\"$lat_num\", \"updatedTime\":\"$update_time\"}" -H "Content-Type: application/json" -X POST $service_url

sleep $time_pause
echo "---------------------------------------------------------------"
done

